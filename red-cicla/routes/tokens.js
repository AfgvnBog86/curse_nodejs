var express = require('express')
var router = express.Router()
var tokenController = require('../controller/token')


router.get('/confirmar/:token', tokenController.confirmarGet);


module.exports = router;
