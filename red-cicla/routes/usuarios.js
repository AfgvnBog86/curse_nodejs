var express = require('express')
var router = express.Router()
var userController = require('../controller/usuarios')

router.get('/', userController.userlist);
router.get('/crear', userController.userNuevoGet);
router.post('/crear', userController.userNuevoPost);
router.post('/borrar', userController.userBorrarPost);
/*
router.get('/:id/editar', userController.userEditarGet);
router.post('/:id/editar', userController.userEditarPost)

*/
module.exports = router;
