var express = require('express')
var router = express.Router()
var BiciController = require('../controller/bicicletas')

router.get('/', BiciController.bicicletalist);
router.get('/crear', BiciController.bicicletNuevoGet);
router.post('/crear', BiciController.bicicletNuevoPost);
router.get('/:id/editar', BiciController.bicicletEditarGet);
router.post('/:id/editar', BiciController.bicicletEditarPost)
router.post('/:id/borrar', BiciController.bicicletaBorrarPost);
module.exports = router;
