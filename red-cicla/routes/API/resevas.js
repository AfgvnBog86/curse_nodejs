var express = require('express')
var router = express.Router()
var BiciController = require('../../controller/API/ReservaControllerApi')

router.post('/crear', BiciController.ReservaCrear);

//router.post('/:id/borrar', BiciController.bicicletaBorrarPost);
module.exports = router;