var express = require('express')
var router = express.Router()
var BiciController = require('../../controller/API/BiciclietaControllerApi')
var authController = require('../../controller/API/authControllerApi')

router.get('/', BiciController.bicicletaList);
router.post('/crear', BiciController.bicicletaCrear);
router.post('/borrar', BiciController.bicicletaBorrar);
//router.post('/:id/borrar', BiciController.bicicletaBorrarPost);
module.exports = router;