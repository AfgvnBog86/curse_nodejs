var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
require('dotenv').config();
var passport = require('./config/passport');
var session = require('express-session');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var biciRouter = require('./routes/bicicletas');
var tokenRouter = require('./routes/tokens');
var biciAPIRouter = require('./routes/API/bicicletas');
var reservaApiRouter = require('./routes/API/resevas');
var authController = require('./controller/API/authControllerApi')

const store = new session.MemoryStore ; 

var app = express();

app.set('secretKey', '12345***H3LL0');

app.use(session({
cookie: {maxAge: 240* 60* 60 * 10000},
store:store,
saveUninitialized:true,
resave: 'true',
secret: '...'

}));


var mongoose = require("mongoose");
const { errorMonitor } = require('stream');
//var mongoDb = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || "mongodb://localhost/red-ciclas";
//var mongoDb = "mongodb+srv://Felipe86:Felipe86@red-bicicletas.qgmdd.mongodb.net/<dbname>?retryWrites=true&w=majority"

var mongoDb = process.env.MONGO_URI 
//mongoose.connect(mongoDb , {useNewUrlParser: true })

mongoose.connect(mongoDb, function (err, res) {
  if (err) {
  console.log ('ERROR connecting to: ' + mongoDb + '. ' + err);
  } else {
  console.log ('Succeeded connected to: ' + mongoDb);
  }
});



mongoose.Promise =global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console , "Mongo Db connection error"))


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/logout',function(req , res ){
  req.logout();
  res.render('session/login')
}) ;

app.get('/login',function(req , res ){
  res.render('session/login')
}) ;

app.post('/login', function(req , res , next){
  passport.authenticate('local', function(err, usuario, info) {
    if (err){next(err)}
    console.log(usuario)
      if(!usuario){ 
              res.redirect('/login' )
      }else{   
         req.login(usuario , function(err){
            if (err){next(err)}
             res.redirect('/usuarios' )
          });
      }
    })(req , res , next);

});

app.get('/logout',function(req , res ,next ){
  req.logout();
  return res.redirect('/usuarios')
}) ;


app.get('/forgotPassword',function(req , res ,next ){
  res.render('session/newpassword')
}) ;

app.use('/', indexRouter);
app.use('/usuarios',loggedIn, usersRouter);
app.use('/bicicletas',loggedIn, biciRouter);
app.use('/token', tokenRouter);

app.use('/bicicletasAPI', validateUser ,biciAPIRouter);
app.use('/reservaAPI', reservaApiRouter);
app.use('/authAPI', authController.authApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn (req,res ,next) {

  if(req.user){
    next();
  }else{
    console.log('ususario no loggeado ');
    res.redirect('/login');
  }

};

function validateUser(req,res,next){
  jwt.verify(req.headers['x-access-token'],req.app.get('secretKey'),function(err, decoded){
    if(err){
      res.json({staus:"error",message:err.message,data:null})
    }else{

      req.body.userId = decoded.id ;
      console.log('jwt verify : ' + decoded)
      next();
    }


  });

}

module.exports = app;
