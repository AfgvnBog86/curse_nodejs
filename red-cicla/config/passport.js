const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
const Usuario = require('../models/usuario')

passport.use(new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password'
      },
    function(email, password, cb) {
      
      Usuario.findByEmail(email, function(err, user) {
        if (err) { return cb(err); }
        if (!user) { return cb(null, false);  }
        if(!user.validPassword(password) ) { return cb(null, false); }
        return cb(null, user);
      });
    }));
  
  
  // Configure Passport authenticated session persistence.
  //
  // In order to restore authentication state across HTTP requests, Passport needs
  // to serialize users into and deserialize users out of the session.  The
  // typical implementation of this is as simple as supplying the user ID when
  // serializing, and querying the user record by ID from the database when
  // deserializing.
  passport.serializeUser(function(user, cb) {
    cb(null, user.id);
  });
  
  passport.deserializeUser(function(id, cb) {
    Usuario.findById(id, function (err, user) {
      if (err) { return cb(err); }
      cb(null, user);
    });
  });
module.exports = passport;