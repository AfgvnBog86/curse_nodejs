var mongoose = require("mongoose")
var Bicicleta= require("../../models/bicicletas");
var Usuario = require("../../models/usuario");
var Reserva = require("../../models/reserva");




describe( ("Testing  reserva all") , () => {

    beforeEach( function (done) {
        var mongoDB = 'mongodb://localhost/red-ciclas'
        mongoose.connect(mongoDB , {useNewUrlParser:true} )
        const db = mongoose.connection;
        db.on('error', console.error.bind(console,"conection error"))
        db.once('open', function(){
            console.log("we are conect to database !! ");
            done();
        });

    });

    afterEach( function(done) {
       
       Reserva.deleteMany({} , function(err , success){
            console.log("delete many reservas ")
            if(err) console.log(err);
            Usuario.deleteMany({} , function(err , success){
                console.log("delete many user ")
                if(err) console.log(err);
                Bicicleta.deleteMany({} , function(err , success){
                    console.log("delete many bicicles ")
                    if(err) console.log(err);
                    done();
                });
            });
        });

    });


    describe( ("Al realizar una nueva reserva ") , () => {
        it('exise la reseva ??? ' ,(done) => {
            const usuario = new Usuario ({nombre:'AndresFgvn'})
            usuario.save();
            const bici = new Bicicleta ( { code:1 ,color:"verde", modelo : "todoerreno"   })
            bici.save();
            var hoy = new  Date();
            var man = new Date();
            man.setDate(hoy.getDate()+1);
            usuario.resevar(bici.id , hoy , man , function(err,reserva){
                Reserva.find({}).populate("bicicleta").populate('usuario').exec(function(err ,reservas){
                    console.log(reservas.length)
                    expect(reservas.length).toBe(1)
                    done()

                })
    
                
   
            });
        });

    });






});