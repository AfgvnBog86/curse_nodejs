var mongoose = require("mongoose")
var Bicicleta = require("../../models/bicicletas");
var server = require('../../bin/www')

describe( ("Testting  Bicicleta.all") , () => {

        beforeEach( function (done) {
            var mongoDB = 'mongodb://localhost/red-ciclas'
            mongoose.connect(mongoDB , {useNewUrlParser:true} )
            const db = mongoose.connection;
            db.on('error', console.error.bind(console,"conection error"))
            db.once('open', function(){
                console.log("we are conect to database !! ");
                done();
            });

        });

        afterEach( function(done) {
            Bicicleta.deleteMany({} , function(err , success){
                console.log("delete many ")
                if(err) console.log(err);
                done();
            });

        });

 



    describe( ("Testting  Bicicleta.create instance") , () => {
        it('Crea insancia' ,() => {

            var bici = Bicicleta.createInstance(1, "verde" , "BMX" , [-57.5,76]);
            // expect(bici.code).toBe(1);
            expect(bici.code).toBe(1);

        });

    });

    describe( ("Testting  Bicicleta.all") , () => {
    
        it('lista vacia' ,(done) => {
            Bicicleta.all( function(err, bicis ) {
                expect(bicis.length).toBe(0)
                done()

            });

        });

    });


    describe( ("Testting  Bicicleta.add") , () => {
    
        it('add elemnt to list ' ,(done) => {
            var bici = new Bicicleta ( { code:1 ,color:"verde", modelo : "todoerreno" ,   })
            Bicicleta.add(bici , function(err, newbici ) {
                Bicicleta.all(  function(err, bicis ){
                    expect(bicis.length).toEqual(1)
                    expect(bicis[0].code).toEqual(bici.code)
                    done()

                });

            
            });

        });

    });

    describe( ("Testting  Bicicleta.findByCode") , () => {
    
        it('add elemnt to list ' ,(done) => {
            var bici = new Bicicleta ( { code:1 ,color:"verde", modelo : "todoerreno" ,   })
            Bicicleta.add(bici , function(err, newbici ) {

                if (err) console.log(err)

                var bici2 = new Bicicleta ( { code:2 ,color:"verde", modelo : "todoerreno" ,   })
                Bicicleta.add(bici2 , function(err, newbici ) {

                    if (err) console.log(err)
                    Bicicleta.findByCode(1 ,function(err ,targetBici){
                    expect(targetBici.code).toBe(bici.code)
                    done()

                    });
                });
            });
        });

    });


    describe( ("Testting  Bicicleta.remove") , () => {
    
        it('add and remove  elemnt to list ' ,(done) => {
            var bici = new Bicicleta ( { code:1 ,color:"verde", modelo : "todoerreno" ,   })
            Bicicleta.add(bici , function(err, newbici ) {

                if (err) console.log(err)

                var bici2 = new Bicicleta ( { code:2 ,color:"verde", modelo : "todoerreno" ,   })
                Bicicleta.add(bici2 , function(err, newbici ) {

                    if (err) console.log(err)
                    Bicicleta.removeByCode(1 ,function(err ,targetBici){
                    Bicicleta.all( function(err, bicis )
                     {
                            expect(bicis.length).toBe(1)
                            done()
            
                        });

                    });
                });
            });
        });

    });




});

/*
beforeEach( () => { console.log("testeando…") });

beforeEach( () => {
    console.log('tesenado')
    Bicicleta.all = []

});


describe( ("Testting  Bicicleta.all") , () => {

    it('Comienza vacia' , () => {

        expect(Bicicletas.all.length).toBe(0)
    });

});

describe( ("Testting  Bicicleta.add") , () => {

    it('Comienza vacia' , () => {

        expect(Bicicletas.all.length).toBe(0)
        var a = new Bicicleta ('1','rojo','cross',['4.750492','-74.084533']) //[4.65049251, -74.084533]
        Bicicleta.add(a)
        expect(Bicicletas.all.length).toBe(1)
        expect(Bicicletas.all[0]).toBe(a)
        
    });

});

describe( ("Testting  Bicicleta.FindBy") , () => {

    it('Prueba de busqueda ' , () => {

        expect(Bicicletas.all.length).toBe(0)
        var a = new Bicicleta ('1','rojo','cross',['4.750492','-74.084533']) //[4.65049251, -74.084533]
        var b = new Bicicleta ('2','verde','cross',['4.50492','-74.4533'])
        Bicicleta.add(a)
        Bicicleta.add(b)
        var temp = Bicicleta.findById(1)
        expect(temp.id).toBe('1')
        expect(temp.color).toBe('rojo')


        
    });

});

describe( ("Testting  Bicicleta.remove") , () => {

    it('Prueba de busqueda ' , () => {

        expect(Bicicletas.all.length).toBe(0)
        var a = new Bicicleta ('1','rojo','cross',['4.750492','-74.084533']) //[4.65049251, -74.084533]
        Bicicleta.add(a)
        Bicicleta.remove(a.id)
        expect(Bicicletas.all.length).toBe(0)

        
    });

});
*/