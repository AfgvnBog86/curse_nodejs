var mongoose  = require("mongoose")
var Schema = mongoose.Schema;

var TokenSchema =  new Schema({
    token: {
        type:String,
        required:true
    }, 
    FechaCreacion: {
        type:Date,
        required:true ,
        expires:43200

    },
    usuario: { 
        type: mongoose.Schema.Types.ObjectId ,
        required:true ,
        ref:'Usuario'
    }
});

TokenSchema.statics.findByid= function(token , cb){
    return  this.findOne  ( {token : token}, cb)
}


module.exports = mongoose.model('Token', TokenSchema);