var mongoose  = require("mongoose");
var Schema = mongoose.Schema;
var Reserva = require('./reserva');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
const validator = require('validator');
var mailer = require('../mailer/mailer');
var Token = require('../models/token');

var uniqueValidator = require('mongoose-unique-validator');
const saltRound = 10 


var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};


var usuarioSchema = new Schema ({
    nombre :{
        type : String,
        trim : true, 
        required :[ true  , 'el nombre es requerido ']
    } ,
    email :{
        type : String,
        trim : true,
        unique : true , 
        required :[ true  , 'el email es requerido '],
        lowercase: true, 
        validate:{
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email',
            isAsync: false
          } },
    password :{
        type : String,
        required :[ true  , 'el password es requerido ']

    },
    passwordResetToken: String,
    passwordResetTokenExpired : Date , 
    verificado :{
        type:Boolean,
        default:false
    }


});

usuarioSchema.statics.createInstance = function(nombre , email , password ){

    return new this ({
        nombre: nombre,
        email:email,
        password:password
    });


}

usuarioSchema.pre('save', function( next ){

        if (this.isModified('password')){
            this.password = bcrypt.hashSync( this.password, saltRound )
        }
        next();


});


usuarioSchema.plugin(uniqueValidator ,{messge: "el path ya existe "});

usuarioSchema.methods.validPassword = function(password){
    console.log('validando password ' + password + '  ' + this.password)
    return bcrypt.compareSync(password,this.password) ;

}

usuarioSchema.methods.resevar = function( biciId , desde , hasta , cb){

var reservar = new Reserva ( {usuario:this._id, bicicleta:biciId , desde: desde , hasta:hasta }) 

reservar.save(cb);
}

usuarioSchema.methods.sendWelcomeEmail= function(cb){

    const token = new Token (  { usuario: this.id  , token: crypto.randomBytes(16).toString('hex') ,FechaCreacion: Date.now()   }) 
    const emaildestination = this.email;
    token.save(function(err) {
        if(err){console.log(err.message)}
    });
    const message = {
        from: 'no-replay<sender@example.com>',
        to: emaildestination ,
        subject: 'Nodemailer is unicode friendly ✔',
        text: 'por fasvor verfica tu cuenta !',
        html: '<p><b>Hello</b> to myself!</p> <link>http://localhost:3000/token/confirmar/'+ token.token + '</link>'
    }
    mailer.sendMail(message, function( err){
        if(err) { 
          console.log("error mail")
          return console.log(err.message)
        }
        console.log("send mail")

    });

    
}



usuarioSchema.statics.all = function(cb){
    return this.find({},cb);
}

usuarioSchema.statics.add = function(user , cb){
    this.create (user, cb)
}

usuarioSchema.statics.findById = function(id , cb){
    return  this.findOne  ( { _id : id}, cb)
}

usuarioSchema.statics.findByEmail= function(email , cb){
    return  this.findOne  ( {email : email}, cb)
}

usuarioSchema.statics.removeByEmail= function(email , cb){
    return  this.deleteOne  ( {email : email}, cb)
}

module.exports = mongoose.model('Usuario', usuarioSchema )