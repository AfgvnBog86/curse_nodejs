var mongoose  = require("mongoose")
var Schema = mongoose.Schema;

var BicicletaSchema =  new Schema({
code: Number,
color:String,
modelo:String,
ubicacion:{
    type : [Number] , index : {type: '2dsphere' , sparse:true}
}


});


BicicletaSchema.statics.createInstance = function(code , color , modelo , ubicacion){

    return new this ({
        code: code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    });


}

BicicletaSchema.statics.all = function(cb){
    return this.find({},cb);
}

BicicletaSchema.statics.add = function(bici , cb){
    this.create (bici, cb)
}

BicicletaSchema.statics.findByCode = function(bici , cb){
    return  this.findOne  ( {code : bici}, cb)
}

BicicletaSchema.statics.removeByCode = function(bici , cb){
    return  this.deleteOne  ( {code : bici}, cb)
}

BicicletaSchema.statics.UpdateByCode = function(bici , data , cb){
    
    return  this.updateOne  ( {code : bici} , data , cb)
}

module.exports = mongoose.model('Bicicleta', BicicletaSchema);

