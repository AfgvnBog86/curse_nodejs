var mongoose = require("mongoose")
var Usuario = require('../models/usuario')



exports.userlist = function(req, res) {
    Usuario.all( function(err, users ) {
      res.render('usuarios/index', { usuarios:users , title: "Usuarios "})
  });
  
  }

  exports.userNuevoGet = function(req, res) {
    
      res.render('usuarios/crear' ,{error:""})
  
  
  }

  exports.userNuevoPost= function(req, res) {

    if (req.body.password == req.body.cpassword){

      var user = new Usuario ( { nombre:req.body.nombre , email: req.body.email, password: req.body.password  })
      console.log(user)
      user.sendWelcomeEmail();
      user.save()
      res.redirect('/usuarios' )

    }
    res.render('usuarios/crear' ,{ error: " error en le password"})


}

exports.userBorrarPost= function(req, res) {
    console.log(req.body)
    Usuario.removeByEmail(req.body.email ,function(err ,targetBici){
      if (err) console.log(err)
      console.log('borrado')
      res.redirect('/usuarios/')
    });
  
  }
