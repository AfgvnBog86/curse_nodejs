var Usuario = require("../../models/usuario");
var passport = require('../../config/passport');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')



exports.authApiRouter= function(req , res ,next) {
  
  Usuario.findByEmail(req.body.email, function(err, user) {
      if (err) { 
        next(err) 
      } else {
        if( user === null  ) {
            res.status(401).json({status:'error' , message:"ususario  invalido" , data:null})
        }else if( user != null && bcrypt.compareSync(req.body.password , user.password) ) {

            const token= jwt.sign({id:user._id }, req.app.get("secretKey") , {expiresIn:'7d'} );
            res.status(200).json({message:"ususario enconrado" , data:{usuario:user , token:token }});
          }else{
            res.status(401).json({status:'error' , message:"usuario invalido 3 " , data:null})
          }
        }
  });
}



/*  est passport 
exports.authApiRouter= function(req , res ,next) {

    passport.authenticate('local', function(err, usuario, info) {
        if (err){next(err)}
        console.log(usuario)
          if(!usuario){ 
            res.status(200).json({error : "usuario no valido"})
                
          } else{   
             req.login(usuario , function(err){
                if (err){next(err)}
                res.status(200).json({error : "usuario valido"} )
            });
          }
        })(req , res , next);

 }

*/