var Bicicleta  = require('../../models/bicicletas')

exports.bicicletaList= function(req , res ) {
 

    Bicicleta.all( function(err, bicis ) {

        res.status(200).json({
            'bicicletas':bicis
        });

    });

}

exports.bicicletaCrear= function(req , res ) {
    
    console.log(req)
    console.log("Punto 22")

    var ubicacion = [req.body.lat , req.body.log]
    var bici = new Bicicleta ( { code:req.body.id ,color:req.body.color, modelo : req.body.modelo ,ubicacion:ubicacion })
        Bicicleta.add(bici , function(err, newbici ) {
            res.status(200).json({
                'bicicletas':newbici
            });
        });


  
}

exports.bicicletaBorrar= function(req , res ) {

    Bicicleta.removeByCode(req.body.id ,function(err ,targetBici){
        if (err) console.log(err)
        console.log(targetBici)
        res.status(200).json({ 
            bici:targetBici   
        });

    });

  
}

