
var Bicicleta = require('../models/bicicletas')



exports.bicicletalist = function(req, res) {
  var temp ;
  Bicicleta.all( function(err, bicis ) {
  
    res.render('bicicletas/index', {bicis:bicis})
});

}

exports.bicicletNuevoGet = function(req, res) {
  
  res.render('bicicletas/crear')
}

exports.bicicletNuevoPost = function(req, res) {
 // var newB = new Bicicleta(req.body.id , req.body.color, req.body.modelo   )
  var ubicacion = [req.body.lat , req.body.log]

  var bici = new Bicicleta ( { code:req.body.id ,color:req.body.color, modelo : req.body.modelo , ubicacion:ubicacion })
  console.log(bici)
  Bicicleta.add (bici , function(err, newbici ) {
    console.log(err)
    
/*
    const message = {
        from: 'Sender Name <sender@example.com>',
        to: 'Recipient <onie.sawayn29@ethereal.email>',
        subject: 'Nodemailer is unicode friendly ✔',cls
        
        text: 'Hello to myself!',
        html: '<p><b>Hello</b> to myself!</p>'
    }
    mailer.sendMail(message, function( err){
        if(err) { 
          console.log("error mail")
          return console.log(err.message)
        }
        console.log("send mail")

    });*/
  console.log(newbici)
  res.redirect('/bicicletas/')
  });

  
}

exports.bicicletaBorrarPost= function(req, res) {
  console
  Bicicleta.removeByCode(req.body.code ,function(err ,targetBici){
    if (err) console.log(err)
    console.log("andres borra " + targetBici.code );
    res.redirect('/bicicletas/')
  });

}


exports.bicicletEditarGet = function(req, res) {

    Bicicleta.findByCode( req.params.id ,function(err , temp){
      if(err) { 
        console.log("error edit")
        return console.log(err.message)
      }
    console.log(temp)
    res.render('bicicletas/editar' , {bici:temp} )

  } );


}

exports.bicicletEditarPost = function(req, res) {
  
  temp = { color:req.body.color , modelo: req.body.modelo ,  ubicacion : [req.body.lat , req.body.log] }
  
  console.log(temp)
  Bicicleta.UpdateByCode( req.params.id , temp  ,function(err ){
    res.redirect('/bicicletas/')

  } );


}